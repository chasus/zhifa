import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

const routes = [
    {
        path: '/main',
        name: 'Main',
        component: () => import('../views/Main'),
        children: [
            {
                path: '/',
                name: 'Home',
                component: () => import('../views/Home')
            }, {
                path: '/lawList',
                name: 'LawList',
                component: () => import('../views/law/Law')
            }, {
                path: '/lawAdd',
                name: 'lawAdd',
                component: () => import('../views/law/Add')
            }, {
                path: '/monitoring',
                name: 'Monitoring',
                component: () => import('../views/Home')
            }, {
                path: '/publicity',
                name: 'Publicity',
                component: () => import('../views/Home')
            }, {
                path: '/deploy',
                name: 'Deploy',
                component: () => import('../views/Home')
            }, {
                path: '/auditList',
                name: 'List',
                component: () => import('../views/audit/list')
            }, {
                path: '/auditAdd',
                name: 'Add',
                component: () => import('../views/audit/add')
            }, {
                path: '/zfztwh',
                name: 'zfztwh',
                component: () => import('../views/deploy/zfztwh')
            }, {
                path: '/zfrywh',
                name: 'zfrywh',
                component: () => import('../views/deploy/zfrywh')
            }, {
                path: '/zfrycx',
                name: 'zfrycx',
                component: () => import('../views/law/zfrywh')
            }, {
                path: '/zfztcx',
                name: 'zfztcx',
                component: () => import('../views/law/zfztwh')
            }
        ]
    }, {
        path: '/bigData',
        name: 'BigData',
        meta: {
            name: '首页',
            icon: 'icon-demo17',
            title: '忻州市行政执法数据中心'
        },
        component: () => import('../views/bigDate/BigData')
    }, {
        path: '/mobileVideo',
        name: 'MobileVideo',
        meta: {
            name: '视频连线',
            icon: 'icon-demo17',
            title: '忻州市行政执法数据中心'
        },
        component: () => import('../views/mobileVideo/Video')
    }, {
        path: '*',
        redirect: '/main',
        name: '404',
        meta: {
            title: '哎呀，页面找不到了！'
        }
        // component: () => import('../views/404')
    }
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

router.beforeEach((to, form, next) => {
    window.document.title = to.meta.title === undefined ? '忻州市行政执法综合管理监督平台' : to.meta.title
    next()
})

export default router
