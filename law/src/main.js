import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import echarts from 'echarts'
import Video from 'video.js'
import 'video.js/dist/video-js.css'
0
Vue.config.productionTip = false
Vue.use(ElementUI)
Vue.prototype.$echarts = echarts
Vue.prototype.$video = Video

new Vue({
    router,
    store,
    render: h => h(App)
}).$mount('#app')
