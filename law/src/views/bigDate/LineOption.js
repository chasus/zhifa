const lineName = ['2020-01', '2020-02', '2020-03', '2020-04', '2020-05', '2020-06', '2020-07', '2020-08']
const lineData1 = lineName.map(() => {
    return Math.round(Math.random() * 500)
})
const lineData2 = lineName.map(() => {
    return Math.round(Math.random() * 500)
})
const lineData3 = lineName.map(() => {
    return Math.round(Math.random() * 500)
})
const lineData4 = lineName.map(() => {
    return Math.round(Math.random() * 500)
})
const lineData5 = lineName.map(() => {
    return Math.round(Math.random() * 500)
})

export default {
    tooltip: {
        trigger: 'axis',
        axisPointer: { // 坐标轴指示器，坐标轴触发有效
            type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
        }
    },
    color: ['#5B8FF9', '#5AD8A6', '#5D7092', '#F6BD16', '#E8684A'],
    legend: {
        data: ['群众举报', '上级交办', '部门移交', '巡查上报', '其他'],
        textStyle: {
            color: '#FFFFFF',
            fontSize: 12
        },
        right: '5%',
        top: '18%'
    },
    grid: {
        top: '45%',
        bottom: '15%',
        right: '5%'
    },
    xAxis: {
        type: 'category',
        boundaryGap: false,
        data: lineName,
        axisLine: {
            show: false
        },
        axisLabel: {
            color: '#FFFFFF'
        }
    },
    yAxis: {
        type: 'value',
        axisLine: {
            show: false
        },
        axisLabel: {
            color: '#FFFFFF'
        },
        splitLine: {
            lineStyle: {
                color: 'rgba(255,255,255,0.1)'
            }
        }
    },
    series: [{
        name: '群众举报',
        data: lineData1,
        type: 'line',
        smooth: true,
        areaStyle: {
            color: {
                type: 'linear',
                x: 0,
                y: 0,
                x2: 0,
                y2: 1,
                colorStops: [{
                    offset: 0, color: 'rgba(91, 143, 250, 0.5)' // 0% 处的颜色
                }, {
                    offset: 1, color: 'rgba(91, 143, 250, 0.1)' // 100% 处的颜色
                }],
                global: false // 缺省为 false
            }
        }
    }, {
        name: '上级交办',
        data: lineData2,
        type: 'line',
        smooth: true,
        areaStyle: {
            color: {
                type: 'linear',
                x: 0,
                y: 0,
                x2: 0,
                y2: 1,
                colorStops: [{
                    offset: 0, color: 'rgba(91, 216, 166, 0.5)' // 0% 处的颜色
                }, {
                    offset: 1, color: 'rgba(91, 216, 166, 0.1)' // 100% 处的颜色
                }],
                global: false // 缺省为 false
            }
        }
    }, {
        name: '部门移交',
        data: lineData3,
        type: 'line',
        smooth: true,
        areaStyle: {
            color: {
                type: 'linear',
                x: 0,
                y: 0,
                x2: 0,
                y2: 1,
                colorStops: [{
                    offset: 0, color: 'rgba(91, 112, 146, 0.5)' // 0% 处的颜色
                }, {
                    offset: 1, color: 'rgba(91, 112, 146, 0.1)' // 100% 处的颜色
                }],
                global: false // 缺省为 false
            }
        }
    }, {
        name: '巡查上报',
        data: lineData4,
        type: 'line',
        smooth: true,
        areaStyle: {
            color: {
                type: 'linear',
                x: 0,
                y: 0,
                x2: 0,
                y2: 1,
                colorStops: [{
                    offset: 0, color: 'rgba(91, 112, 146, 0.5)' // 0% 处的颜色
                }, {
                    offset: 1, color: 'rgba(91, 112, 146, 0.1)' // 100% 处的颜色
                }],
                global: false // 缺省为 false
            }
        }
    }, {
        name: '其他',
        data: lineData5,
        type: 'line',
        smooth: true,
        areaStyle: {
            color: {
                type: 'linear',
                x: 0,
                y: 0,
                x2: 0,
                y2: 1,
                colorStops: [{
                    offset: 0, color: 'rgba(232, 100, 75, 0.5)' // 0% 处的颜色
                }, {
                    offset: 1, color: 'rgba(232, 100, 75, 0.1)' // 100% 处的颜色
                }],
                global: false // 缺省为 false
            }
        }
    }]
}
