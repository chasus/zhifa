export default {
    color: ['#5B8FF9', '#5AD8A6', '#5D7092', '#F6BD16', '#E8684A', '#6DC8EC'],
    tooltip: {
        trigger: 'item',
        formatter: '{a} <br/>{b} : {c} ({d}%)'
    },
    legend: {
        orient: 'vertical',
        right: 30,
        top: 'center',
        data: ['行政许可', '行政检查', '行政处罚', '行政强制', '行政征收征用', '其它'],
        textStyle: {
            color: '#FFFFFF',
            fontSize: 12
        },
        itemGap: 15
    },
    series: [
        {
            name: '法制监督',
            type: 'pie',
            radius: [45, 80],
            center: ['32%', '55%'],
            data: [
                { value: 518, name: '行政许可' },
                { value: 769, name: '行政检查' },
                { value: 897, name: '行政处罚' },
                { value: 421, name: '行政强制' },
                { value: 1342, name: '行政征收征用' },
                { value: 590, name: '其它' }
            ],
            label: {
                show: true,
                position: 'inside',
                formatter: '{c}',
                fontSize: 12
            }
        }
    ]
}
