export default {
    color: '#5B8FF9',
    grid: {
        left: '15%',
        bottom: '5%',
        right: '5%'
    },
    tooltip: {
        trigger: 'axis',
        axisPointer: { // 坐标轴指示器，坐标轴触发有效
            type: 'shadow' // 默认为直线，可选为：'line' | 'shadow'
        }
    },
    xAxis: {
        type: 'category',
        data: '',
        axisLine: {
            show: false
        },
        axisTick: {
            show: false
        },
        axisLabel: {
            inside: true,
            rotate: 90,
            color: '#FFFFFF',
            fontSize: 12
        },
        z: 10
    },
    yAxis: {
        type: 'value',
        axisLine: {
            show: false
        },
        axisTick: {
            show: false
        },
        axisLabel: {
            color: '#FFFFFF'
        },
        splitLine: {
            lineStyle: {
                color: 'rgba(255,255,255,0.1)'
            }
        }
    },
    dataZoom: [
        {
            type: 'inside'
        }
    ],
    series: [{
        data: '',
        type: 'bar',
        barWidth: '50%',
        itemStyle: {
            normal: {
                barBorderRadius: [50, 50, 0, 0]
            }
        }
    }]

}
