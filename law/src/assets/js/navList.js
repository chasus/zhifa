const navList = [
    {
        path: '/main',
        name: '首页',
        icon: 'icon-demo17'
    }, {
        path: '/law',
        name: '行政执法',
        icon: 'icon-zhifa',
        children: [
            {
                path: '/lawRecord',
                name: '行政执法全过程记录',
                children: [
                    {
                        path: '/lawList',
                        name: '执法全过程维护'
                    }
                ]
            }, {
                path: '/audit',
                name: '重大行政执法决定法制审核',
                children: [{
                    path: '/auditList',
                    name: '法制审核意见'
                }]
            }, {
                path: '/c',
                name: '行政执法综合查询',
                children: [
                    {
                        path: '/zfztcx',
                        name: '执法主体综合查询'
                    }, {
                        path: '/zfrycx',
                        name: '执法人员综合查询'
                    }
                ]
            }
        ]
    }, {
        path: '/monitoring',
        name: '监控中心',
        icon: 'icon-jiankong',
        children: [
            { path: '/bigData', name: '数据中心' },
            {
                path: '/a',
                name: '执法统计',
                children: [
                    { path: '/a', name: '案件区域分布统计' },
                    { path: '/b', name: '案件主体部门统计' },
                    { path: '/c', name: '案件执法结果类型统计' },
                    { path: '/d', name: '执法信息完整度统计' }
                ]
            }
        ]

    }, {
        path: '/publicity',
        name: '公示中心',
        icon: 'icon-gongshi',
        children: [
            {
                path: '/a',
                name: '行政执法公示管理',
                children: [
                    { path: '/b', name: '公示通知' },
                    { path: '/c', name: '行政执法机关' },
                    { path: '/d', name: '行政执法人员' },
                    { path: '/e', name: '行政权利事项清单' },
                    { path: '/f', name: '随机抽查事项清单' },
                    { path: '/g', name: '行政执法事项清单' },
                    { path: '/h', name: '执法流程图' },
                    { path: '/i', name: '行政执法服务指南' },
                    { path: '/j', name: '行政许可信息公示' },
                    { path: '/k', name: '行政检查信息公示' },
                    { path: '/l', name: '行政执法信息公示' },
                    { path: '/m', name: '行政强制信息公示' },
                    { path: '/n', name: '其他信息公示' }
                ]
            }, {
                path: '/b',
                name: '执法事项管理',
                children: [
                    { path: '/c', name: '权责清单' },
                    { path: '/d', name: '行政检查' },
                    { path: '/e', name: '行政许可' },
                    { path: '/f', name: '行政执法' },
                    { path: '/g', name: '行政强制' }
                ]
            }, { path: '/c', name: '法律法规' }
        ]
    }, {
        path: '/deploy',
        name: '配置中心',
        icon: 'icon-peizhi',
        children: [
            {
                path: '/a',
                name: '行政执法要素管理',
                children: [
                    { path: '/b', name: '监督机关维护' },
                    { path: '/c', name: '监督机关查询' },
                    { path: '/d', name: '监督人员维护' },
                    { path: '/e', name: '监督人员查询' },
                    { path: '/f', name: '执法部门维护' },
                    { path: '/g', name: '执法部门查询' },
                    { path: '/zfrywh', name: '执法人员维护' },
                    { path: '/i', name: '执法人员查询' },
                    { path: '/zfztwh', name: '执法主体维护' },
                    { path: '/k', name: '受委托单位' }
                ]
            },
            {
                path: '/b',
                name: '系统管理',
                children: [
                    { path: '/ba', name: '组织机构管理' },
                    { path: '/bb', name: '系统角色管理' },
                    { path: '/bc', name: '菜单管理' },
                    { path: '/bd', name: '定时任务' },
                    { path: '/be', name: '系统参数' },
                    { path: '/bf', name: '系统日志' },
                    { path: '/bg', name: '字典管理' }
                ]
            }, {
                path: '/a',
                name: '法制审核目录清单'
            }
        ]
    }]

export default navList
